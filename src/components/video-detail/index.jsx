import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { ApiService } from "../../service/api.service";
import { Avatar, Box, Chip, Stack, Typography } from "@mui/material";
import ReactPlayer from "react-player";
import {
  CheckCircle,
  FavoriteOutlined,
  MarkChatRead,
  Tag,
  Visibility,
} from "@mui/icons-material";
import Loader from "../loader";
import Videos from "../videos";
// import renderHTML from "react-render-html";

const VideoDetail = () => {
  const [video, setVideo] = useState();
  const [suggetedVideos, setSuggetedVideos] = useState();
  const { id } = useParams();

  useEffect(() => {
    try {
      const getVideo = async () => {
        const videos = await ApiService.fetching(
          `videos?part=snippet,statistics&id=${id}`
        );

        setVideo(videos.items[0]);
      };
      const suggestedVideos = async () => {
        const videos = await ApiService.fetching(
          `search?part=snippet&relatedToVideoId=${id}&type=video`
        );

        setSuggetedVideos(videos.items);
      };

      getVideo();
      suggestedVideos();
    } catch (error) {}
  }, [id]);

  if (!video) return <Loader />;
  if (!suggetedVideos) return <Loader />;

  return (
    <Box minHeight={"90vh"} mb={10}>
      <Box display={"flex"} sx={{ flexDirection: { xs: "column", md: "row" } }}>
        <Box width={{ xs: "100%", md: "75%" }}>
          <ReactPlayer
            url={`https://www.youtube.com/watch?v=${id}`}
            className="react-player"
            controls
          />
          {video.snippet.tags.map((tag, idx) => (
            <Chip
              label={tag}
              key={idx}
              variant="outlined"
              sx={{ marginTop: "10px", cursor: "pointer", ml: "10px" }}
              deleteIcon={<Tag />}
              onDelete={() => {}}
            />
          ))}
          <Typography variant="h5" fontWeight={"bold"} p={2}>
            {video?.snippet?.title}
          </Typography>

          <Typography variant="subtitle2" sx={{ opacity: ".7" }} p={2}>
            {/* {renderHTML(video?.snippet?.description)} */}
            {video?.snippet?.description}
          </Typography>
          <Stack
            direction={"row"}
            gap={"20px"}
            alignItems={"center"}
            py={1}
            px={2}
          >
            <Stack
              sx={{ opacity: 0.7 }}
              direction={"row"}
              alignItems={"center"}
              gap={"3px"}
            >
              <Visibility />
              {parseInt(video?.statistics?.viewCount).toLocaleString()} views
            </Stack>
            <Stack
              sx={{ opacity: 0.7 }}
              direction={"row"}
              alignItems={"center"}
              gap={"3px"}
            >
              <FavoriteOutlined />
              {parseInt(video?.statistics?.likeCount).toLocaleString()} likes
            </Stack>
            <Stack
              sx={{ opacity: 0.7 }}
              direction={"row"}
              alignItems={"center"}
              gap={"3px"}
            >
              <MarkChatRead />
              {parseInt(video?.statistics?.commentCount).toLocaleString()}{" "}
              comments
            </Stack>
          </Stack>
          <Link to={`/channels/${video?.snippet.channelId}`}>
            <Stack direction={"row"} py={1} px={2}>
              <Stack
                direction={"row"}
                alignItems={"center"}
                gap={"5px"}
                marginTop={"5px"}
              >
                <Avatar
                  alt={video.snippet.channelTitle}
                  src={video.snippet.thumbnails.default.url}
                />
                <Typography variant="subtitle2" color="bold">
                  {video?.snippet?.channelTitle}
                  <CheckCircle
                    sx={{ fontSize: "12px", color: "gray", ml: "5px" }}
                  />
                </Typography>
              </Stack>
            </Stack>
          </Link>
        </Box>
        <Box
          width={{ xs: "100%", md: "25%" }}
          px={2}
          py={{ md: 1, xs: 5 }}
          justifyContent={"center"}
          alignItems={"center"}
          overflow={"scroll"}
          maxHeight={"120vh"}
        >
          <Videos videos={suggetedVideos && suggetedVideos} />
        </Box>
      </Box>
    </Box>
  );
};

export default VideoDetail;
