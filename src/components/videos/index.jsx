import { Box, Stack } from "@mui/material";
import VideoCard from "../video-card";
import ChannelCard from "../channel-card";
import Loader from "../loader";

const Videos = ({ videos }) => {
  if (!videos.length) return <Loader />;

  return (
    <Stack
      width={"100%"}
      direction={"row"}
      flexWrap={"wrap"}
      justifyContent={"start"}
      alignItems={"center"}
      gap={2}
    >
      {videos.map((video) => (
        <Box key={video.id.videoId}>
          {video.id.videoId && <VideoCard video={video} />}
          {video.id.channelId && <ChannelCard video={video} />}
        </Box>
      ))}
    </Stack>
  );
};

export default Videos;
