import { Routes, Route } from "react-router-dom";
import { Box } from "@mui/material";
import Main from "../main";
import Channel from "../channel";
import VideoDetail from "../video-detail";
import Search from "../search";
import Navbar from "../navbar";

const App = () => {
  return (
    <Box>
      <Navbar />
      <Routes>
        <Route path="/" element={<Main />} />
        <Route path="/channels/:id" element={<Channel />} />
        <Route path="/videos/:id" element={<VideoDetail />} />
        <Route path="/search/:id" element={<Search />} />
      </Routes>
    </Box>
  );
};

export default App;
