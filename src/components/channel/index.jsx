import { Box, Button, Container } from "@mui/material";
import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import ChannelCard from "../channel-card";
import { ApiService } from "../../service/api.service";
import Videos from "../videos/index";

const Channel = () => {
  const [channel, setChannel] = useState();
  const [videos, setVideos] = useState([]);

  const { id } = useParams();

  useEffect(() => {
    const getChannel = async () => {
      const channel = await ApiService.fetching(
        `channels?part=snippet&id=${id}`
      );

      setChannel(channel.items[0]);
    };

    const getVideos = async () => {
      const videos = await ApiService.fetching(
        `search?channelId=${id}&part=snippet%2Cid&order=date`
      );

      setVideos(videos);
    };

    getChannel();
    getVideos();
  }, [id]);
  return (
    <Box minHeight={"95vh"} mt={"10vh"}>
      <Box>
        <Box
          width={"100%"}
          height={"200px"}
          zIndex={10}
          sx={{
            backgroundImage: `url(${channel?.brandingSettings?.image?.bannerExternalUrl})`,
            backgroundPosition: "center",
            backgroundSize: "cover",
            objectFit: "cover",
            backgroundRepeat: "no-repeat",
          }}
        />
        <ChannelCard video={channel} marginTop={"-100px"} />
      </Box>
      <Container maxWidth={"90%"}>
        <Videos videos={videos}></Videos>
      </Container>
    </Box>
  );
};

export default Channel;
